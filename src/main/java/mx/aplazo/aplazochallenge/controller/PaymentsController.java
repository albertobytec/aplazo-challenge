package mx.aplazo.aplazochallenge.controller;

import mx.aplazo.aplazochallenge.facade.PaymentFacade;
import mx.aplazo.aplazochallenge.request.PaymentRequest;
import mx.aplazo.aplazochallenge.response.PaymentResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("aplazo/payments")
public class PaymentsController {

    private final PaymentFacade paymentFacade;

    public PaymentsController(PaymentFacade paymentFacade) {
        this.paymentFacade = paymentFacade;
    }

    @PostMapping("/generate-payments")
    public ResponseEntity<PaymentResponse> generatePayments(@RequestBody PaymentRequest paymentRequest) {

        return new ResponseEntity<>(paymentFacade.calculatePayments(paymentRequest), HttpStatus.CREATED);
    }
}
