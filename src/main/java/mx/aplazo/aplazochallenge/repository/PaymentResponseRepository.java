package mx.aplazo.aplazochallenge.repository;

import mx.aplazo.aplazochallenge.response.PaymentResponse;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PaymentResponseRepository extends JpaRepository<PaymentResponse, Long> {
}
