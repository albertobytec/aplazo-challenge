package mx.aplazo.aplazochallenge.repository;

import mx.aplazo.aplazochallenge.request.PaymentRequest;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PaymentRequestRepository extends JpaRepository<PaymentRequest, Long> {
}
