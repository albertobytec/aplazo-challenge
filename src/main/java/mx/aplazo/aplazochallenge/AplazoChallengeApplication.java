package mx.aplazo.aplazochallenge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AplazoChallengeApplication {

    public static void main(String[] args) {
        SpringApplication.run(AplazoChallengeApplication.class, args);
    }

}
