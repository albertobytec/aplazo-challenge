package mx.aplazo.aplazochallenge.service;

import mx.aplazo.aplazochallenge.request.PaymentRequest;
import mx.aplazo.aplazochallenge.response.PaymentResponse;

public interface PaymentsService {

    PaymentResponse calculatePayments(PaymentRequest paymentsRequest);
    PaymentRequest savePaymentRequest(PaymentRequest paymentRequest);
    PaymentResponse savePaymentResponse(PaymentResponse paymentResponse);
}
