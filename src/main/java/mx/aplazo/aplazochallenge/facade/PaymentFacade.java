package mx.aplazo.aplazochallenge.facade;

import mx.aplazo.aplazochallenge.request.PaymentRequest;
import mx.aplazo.aplazochallenge.response.PaymentResponse;
import mx.aplazo.aplazochallenge.service.PaymentsService;
import org.springframework.stereotype.Component;

@Component
public class PaymentFacade {

    private final PaymentsService paymentsService;

    public PaymentFacade(PaymentsService paymentsService) {
        this.paymentsService = paymentsService;
    }

    public PaymentResponse calculatePayments(PaymentRequest paymentRequest){

        PaymentResponse paymentResponse = paymentsService.calculatePayments(paymentRequest);
        PaymentResponse paymentResponseSaved = paymentsService.savePaymentResponse(paymentResponse);
        paymentRequest.setResponse(paymentResponseSaved);
        paymentsService.savePaymentRequest(paymentRequest);

        return paymentResponseSaved;
    }
}
