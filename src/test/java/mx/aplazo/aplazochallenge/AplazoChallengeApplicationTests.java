package mx.aplazo.aplazochallenge;

import mx.aplazo.aplazochallenge.request.PaymentRequest;
import mx.aplazo.aplazochallenge.response.Payment;
import mx.aplazo.aplazochallenge.response.PaymentResponse;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class AplazoChallengeApplicationTests {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    void generatePaymentsSuccessfully() {
        //given
        PaymentRequest paymentRequest = buildPaymentRequest();

        //when
        ResponseEntity<PaymentResponse> paymentResponse = restTemplate.postForEntity("http://localhost:" +
                port + "/aplazo/payments/generate-payments", paymentRequest, PaymentResponse.class);

        //then
        Assertions.assertEquals(201, paymentResponse.getStatusCode().value());
        Assertions.assertNotNull(Objects.requireNonNull(paymentResponse.getBody()).getPayments());
        Assertions.assertEquals(paymentRequest.getTerms(), paymentResponse.getBody().getPayments().size());

        List<Integer> expectedPaymentNumbers = IntStream.rangeClosed(1, 10).boxed().collect(Collectors.toList());
        List<Integer> actualPaymentNumbers = paymentResponse.getBody().getPayments().stream().
                map(Payment::getPaymentNumber).collect(Collectors.toList());

        Assertions.assertEquals(expectedPaymentNumbers, actualPaymentNumbers);

        //sum of the amount of each payment is equal to the following calculation: ((amount/terms) + (amount * rate)) * terms
        Double totalAmountExpected = ((paymentRequest.getAmount() / paymentRequest.getTerms() +
                paymentRequest.getAmount() * paymentRequest.getRate()) * paymentRequest.getTerms());

        Double totalAmountActual = paymentResponse.getBody().getPayments().stream().map(Payment::getAmount).reduce(0.0, Double::sum);
        Assertions.assertEquals(totalAmountExpected, totalAmountActual);
    }

    @Test
    void healthSuccessfully(){
        //when
        ResponseEntity<String> paymentResponse = restTemplate.getForEntity("http://localhost:" +
                port + "/aplazo/health", String.class);
        //then
        Assertions.assertTrue(paymentResponse.getStatusCode().is2xxSuccessful());

    }

    private PaymentRequest buildPaymentRequest() {
        PaymentRequest paymentRequest = new PaymentRequest();
        paymentRequest.setAmount(10000.0);
        paymentRequest.setTerms(10);
        paymentRequest.setRate(0.01);

        return paymentRequest;
    }

}
