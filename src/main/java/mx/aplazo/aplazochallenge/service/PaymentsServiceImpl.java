package mx.aplazo.aplazochallenge.service;

import mx.aplazo.aplazochallenge.repository.PaymentRequestRepository;
import mx.aplazo.aplazochallenge.repository.PaymentResponseRepository;
import mx.aplazo.aplazochallenge.request.PaymentRequest;
import mx.aplazo.aplazochallenge.response.Payment;
import mx.aplazo.aplazochallenge.response.PaymentResponse;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service
public class PaymentsServiceImpl implements PaymentsService {

    private final PaymentRequestRepository paymentRequestRepository;
    private final PaymentResponseRepository paymentResponseRepository;
    private static final Integer DAYS_TERMS = 7;

    public PaymentsServiceImpl(PaymentRequestRepository paymentRequestRepository, PaymentResponseRepository paymentResponseRepository) {
        this.paymentRequestRepository = paymentRequestRepository;
        this.paymentResponseRepository = paymentResponseRepository;
    }

    @Override
    public PaymentResponse calculatePayments(PaymentRequest paymentsRequest) {

        Double amountInterest = paymentsRequest.getAmount() * paymentsRequest.getRate();
        Double singlePayment = paymentsRequest.getAmount() / paymentsRequest.getTerms();
        Date currentDate = new Date();
        Calendar calendar = Calendar.getInstance();

        PaymentResponse paymentResponse = new PaymentResponse();

        List<Payment> payments = new ArrayList<>();
        for (int i = 0; i < paymentsRequest.getTerms(); i++) {
            calendar.setTime(currentDate);
            Payment payment = new Payment();
            payment.setPaymentNumber(i + 1);
            payment.setAmount(singlePayment + amountInterest);
            calendar.add(Calendar.DATE, DAYS_TERMS * i);
            payment.setPaymentDate(calendar.getTime());
            payments.add(payment);
        }

        paymentResponse.setPayments(payments);

        return paymentResponse;
    }

    @Override
    public PaymentRequest savePaymentRequest(PaymentRequest paymentRequest) {
        return paymentRequestRepository.save(paymentRequest);
    }

    @Override
    public PaymentResponse savePaymentResponse(PaymentResponse paymentResponse) {
        return paymentResponseRepository.save(paymentResponse);
    }
}
